/** Modules ======================================================== */
import * as express from 'express';
import { json, urlencoded } from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as path from 'path';
import * as compression from 'compression';
/** Routes conf ==================================================== */
import { publicRouter } from './routes/public';
import { protectedRouter } from './routes/protected';
/** New Express Instance =========================================== */
const app: express.Application = express();
/** App setup ====================================================== */
app.disable('x-powered-by');
app.use(json());
app.use(compression());
app.use(urlencoded({ extended: true }));
app.use(cookieParser());
app.use(function(req: express.Request, res: express.Response, next: express.NextFunction) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE');
  next();
});
/** Routes setup =================================================== */
app.use('/api/public', publicRouter);
app.use('/api/secure', protectedRouter);
/** In production mode run application from dist folder ============ */
if (app.get('env') === 'production') {
  app.use(express.static(path.join(__dirname, '/../client')));
}
/** Development error handler, stacktrace ========================== */
if(app.get('env') === 'development') {
  app.use(function(err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err
    });
  });
}
/** Catch 404 and forward to error handler ========================= */
app.use(function(req: express.Request, res: express.Response, next) {
  let err = new Error('Not Found');
  next(err);
});
/** Production error handler, no stacktrace ======================== */
app.use(function(err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
  res.status(err.status || 500);
  res.json({
    error: {},
    message: err.message
  });
});

export { app }