import { Router, Response, Request } from 'express';

const publicRouter: Router = Router();

publicRouter.get('/simple', (request: Request, response: Response) => {
  response.json({
    title: 'Hey.',
    text: 'Hello Api Rest'
  });
});

export { publicRouter }